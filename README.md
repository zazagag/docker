#List of images.

##Base environment.
Very simply base environment. [[zazagag/base]](./base/README.md)

Based on [phusion/baseimage](https://github.com/phusion/baseimage-docker)

####Inside:
* ubuntu 16.04
* node 6.x
* nginx
* ssh
* mc
* git
* net-tools
* realpath
* sudo
