# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [0.8] - 2017-11-06

### Removed
- nginx
- realpath
- git
- net-tools

## [0.1] - Initial
### Added
- realpath 
- git 
- mc 
- sudo 
- net-tools 
- vim
- nginx
- node
- ssh
- vim theme xoria
- bash aliases
