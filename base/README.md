# Base environment.

Based on [phusion/baseimage](https://github.com/phusion/baseimage-docker)

#### Inside:

* ubuntu 16.04
* node 8.x
* npm
* yarn
* nginx
* ssh
* mc
* git
* net-tools
* realpath
* sudo

#### Docker.

Docker Command|Description
|---|---|
|Build from Dockerfile|`docker build -t zazagag/base .`|
|Pull image|`docker pull zazagag/base`|
|Start container|`docker run -d -p 80:80 -p 2222:22 -e DOCKER_SSH_PUBKEY="$(cat ~/.ssh/id_rsa.pub)" --name base zazagag/base`|

#### Users

`root`

`www-data`

#### SSH

SSH server run on `2222` port.

`ssh -p 2222 root@<ip>`

`ssh -p 2222 www-data@<ip>`

#### Nginx.

Nginx gets config by including `/var/www/master/config/nginx*.conf;`.
So any changes in config file should be updating by restarting Nginx.

Nginx Command|Description
|---|---|
|`sudo servive nginx restart`|Restart Nginx.|
|`sudo servive nginx stop`|Stop Nginx.|
|`sudo servive nginx start`|Start Nginx.|
|`sudo servive nginx status`|Status Nginx.|
|`sudo nginx -t`|Check config Nginx.|

#### ~/.profile.

All specified user options are specified in `./build/conf/.profile`.

#### Bash scripts.

Has some bash aliases built-in which stored in `base/build/conf/.bash`:

Alias|Description
|---|---|
|*up*|`git add . && git reset --hard origin/$name && git pull origin $name`|
|*st*|`git status`|
|*go*|`git checkout $arg`|
|*push*|`git checkout $arg`|
|*rbs*|`git rebase -i master`|
|*rbs:c*|`git rebase --continue`|
|*rbs:a*|`git rebase --abort`|
|*npml*|list of npm modules by depth|

#### Tag & Push

```
docker images
docker tag x zazagag/base:0.3
docker login hub.docker.com
docker push zazagag/base:0.3
docker push zazagag/base
```
