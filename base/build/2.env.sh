#!/bin/sh

set -e

mkdir -p /var/www
mkdir -p /var/www/.bash
mkdir -p /var/www/master

cp /build/conf/.profile /var/www/.profile
cp /build/conf/.vimrc /var/www/.vimrc
cp -r /build/conf/.bash /var/www

cp -r /build/conf/.mc /var/www/.mc
cp -r /build/conf/.mc /root/.mc

cp -r /build/conf/.vim /var/www/.vim
cp -r /build/conf/.vim /root/.vim

chown www-data:www-data -R /var/www

echo "www-data ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers

mv /build/conf/base-init.sh /etc/my_init.d/base-init.sh
