#!/bin/sh

set -e

if [ -e /.base-initialized ]; then
    exit 0
fi

echo "USER=www-data" >> /var/www/.profile

echo "export APP_ROOT=/var/www/master/" >> /var/www/.profile

mkdir -p /var/www/.ssh
mkdir -p /root/.ssh
mkdir -p /var/www/master

chown www-data:www-data -R /var/www/.ssh
chmod go-rwx -R /var/www/.ssh
chmod go-rwx -R /root/.ssh

touch /.base-initialized
