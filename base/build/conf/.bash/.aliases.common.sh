#!/bin/bash

# set info by default
export LESS='-eirMX'
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"

export HISTSIZE=10000                      # 500 is default
export HISTFILESIZE=1000000
export HISTTIMEFORMAT='%b %d %I:%M %p '    # using strftime format
export HISTCONTROL=ignoreboth              # ignoredups:ignorespace
export HISTIGNORE="history:pwd:exit:df:ls:ls -la:ll"
export EDITOR="vim"

# colorize output
alias ll='ls -lahG'
alias h='history'

# can also redefine a command to add options
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
alias df='df -h'
alias du='du -h'
alias mkdir='mkdir -p'

# can be used to fix common typos you make
alias pdw='pwd'

alias cdf='cd ~/exness/dev-env/build/private_site/frontendsource';
alias cdp='cd ~/exness/dev-env/build/private_site/pages';
alias cdpp='cd ~/exness/dev-env/build/public_site/pages';
alias cds='cd ~/exness/dev-env/build/private_site/static_member';
alias cdss='cd ~/exness/dev-env/build/public_site/static';
alias cdt='cd ~/exness/dev-env/build/private_site/templates';
alias cdtt='cd ~/exness/dev-env/build/public_site/templates';
alias cdpy='cd ~/exness/dev-env/build/private_site/py';
alias mc='. /usr/lib/mc/mc-wrapper.sh'

# sets terminal title
settitle() {
    echo -n -e "\033]0;$1\007"
}

# list of npm modules by depth
npml() {
    if [ -z "$1" ]; then
        depth="0"
    else
        depth="$1"
    fi
    
    echo "------------------------------";
    echo "npm list --depth=$depth";
    echo "------------------------------";
    npm list --depth=$depth;
}

# recursive delete dir
rm:d() {
   if [ -z "$1" ]; then
    echo "What should I do?"
    return;
    fi;
    
    dir="$1"
    
    echo "------------------------------";
    echo "rm -Rf $dir";
    echo "------------------------------";
    rm -Rf $dir;
}
