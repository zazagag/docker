#!/bin/bash

alias rs='git add . && git reset --hard'
alias pl='git add . && git pull'
alias gorigin='git config --get remote.origin.url'

git:init() {
    git config --global user.email "zazagag@gmail.com"
    git config --global user.name "Andrei Nikitin"
}

function up() {
    if [ -z "$1" ]; then
        name=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')
    else
        name="$1"
    fi

    echo "----------------------"
    echo "git add . && git reset --hard origin/$name && git pull origin $name"
    echo "----------------------"

    git add . && git reset --hard "origin/$name" && git pull origin $name
}

function go() {
    echo "----------------------"
    echo "git checkout $@"
    echo "----------------------"

    git checkout $@
}

function push() {
    echo "----------------------"
    echo "git push $@"
    echo "----------------------"

    git push "$@"
}

function rbs() {
    echo "----------------------"
    echo "git rebase -i master"
    echo "----------------------"

    git rebase -i master
}

function rbs:c() {
    echo "----------------------"
    echo "git rebase --continue"
    echo "----------------------"

    git rebase --continue
}

function rbs:a() {
    echo "----------------------"
    echo "git rebase --abort"
    echo "----------------------"

    git rebase --continue
}

function st() {
    echo "----------------------"
    echo "git status"
    echo "----------------------"

    git status
}

git_uncommit() {
  local status=$(git status --porcelain 2> /dev/null);
  if [[ "$status" != "" ]]; then
    echo '*'
  else
    echo ''
  fi
}
