#!/bin/bash

# http://henrik.nyh.se/2008/12/git-dirty-prompt
# http://www.simplisticcomplexity.com/2008/03/13/show-your-git-branch-name-in-your-prompt/
#   username@Machine ~/dev/dir [master]$   # clean working directory green
#   username@Machine ~/dev/dir [master*]$  # dirty working directory red*
#
function git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
# http://unix.stackexchange.com/questions/88307/escape-sequences-with-echo-e-in-different-shells
function markup_git_branch {
  if [[ "x$1" = "x" ]]; then
    echo -e "$1"
  else
    if [[ $(git status 2> /dev/null | tail -n1) = "nothing to commit, working directory clean" ]]; then
      echo -e "\033[38;5;34m("$1")$(tput sgr0)"
    else
      echo -e "\033[38;5;160m("$1"*)$(tput sgr0)"
    fi
  fi
}

export PS1='\[\033[38;5;106m\][$APP_NAME]\[$(tput sgr0)\]\[\033[38;5;40m\]\u@\[$(tput sgr0)\]\[\033[38;5;14m\]\w\[$(tput sgr0)\] $(markup_git_branch $(git_branch))\[\033[38;5;40m\]$\[$(tput sgr0)\] '

source ~/.bash/.aliases.common.sh
source ~/.bash/.aliases.git.sh
