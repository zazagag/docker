export TERM=xterm-256color

export LANG=en_US.UTF-8

export HISTCONTROL=ignoredups

export PAGER="less -M +Gg"

export APPLICATION_HOSTNAME=docker

export IS_IN_DOCKER=1

cd /var/www/master

source ~/.bash/.aliases.sh
